# README #

### What is this repository for? ###

* This repository hosts a simple header file for programming by contract in C++11. 
It offers services for pre- (expect), post- (ensure) and class invariant conditions.

* Current version: 0.3 (alpha)

### How do I get set up? ###

Setup is very easy since there is only one header file (contracts.h) to include.

To configure the use of Contract, just follow this example:

```

#!c++

// Define DEFINE_CONTRACTS before including contracts.h in a single translation unit.
// Choose the one with main() for example.
#define DEFINE_CONTRACTS
#include "contracts.h"  // ...use correct path for your setup!

```

### How to define a function that use contract? ###

*```CONTRACT_FUNC( return_type, signature )``` defines the function.

*```CONTRACT_BEGIN``` begins the function. Must be followed by ```{/*function body;*/}```.

*```CONTRACT_END``` or ```CONTRACT_END_VOID``` depending on the return type (void or not). Must follow
```{/*function body;*/}```.

```

#!c++

// Few examples of functions we may define...

/**
 * Function that uses contract with no condition... purely useless! 
 */
CONTRACT_FUNC( void, func1( int a ) )
CONTRACT_BEGIN
{
    globalValue = 2*a;
}
CONTRACT_END_VOID

// Another one...
CONTRACT_FUNC( int, func2( int a ) )
CONTRACT_BEGIN
{
    int b = 2*a;
    return b;
}
CONTRACT_END
```

#### Setting conditions... ####

In a function, we can declare **precondition(s)** (conditions that the function expects to be fulfilled) and
**postcondition(s)** (conditions that the function ensures to be fulfilled).

Pre- and postconditions are declared with __condition, variable1[, ...]__ that is, a condition and a list of
one or more variable to be listed if the condition is not fulfilled. For example, we can use (a == 10, a) and
the program will write that "a == 10" failed for "a = 6"...

* ```CONTRACT_EXPECT(condition, variable1[, ...])``` declares a **precondition**.
* ```CONTRACT_ENSURE(condition, variable1[, ...])``` declares a **postcondition**.
* ```CONTRACT_RET_VAL``` is the returned value to be used in a postcondition.

Note than more than one pre- or postconditions can be declared. They must be declared
**before** calling ```CONTRACT_BEGIN```.

```

#!c++

// Simple pre- and postconditions

/**
 * Function that returns its argument.
 *
 * \param a a value...
 * \param b a value...
 * \return a...
 *
 * \pre a < 10
 * \pre a == b
 * \post returned value != 0
 */
CONTRACT_FUNC( int, func1( int a, int b ) )
CONTRACT_EXPECT( a < 10, a )
CONTRACT_EXPECT( a == b, a, b )
CONTRACT_ENSURE( CONTRACT_RET_VALUE != 0, CONTRACT_RET_VALUE )
CONTRACT_BEGIN
{
    return a;
}
CONTRACT_END

```

The previous function will generate precondition errors if **a >= 10** or if **b != a**.
It will also generate a postcondition error if the **return value != 0** (i.e. if a != 0).

**What happens if we want to use an initial argument value in a postcondition if this
value is modified during the execution of the function?**

If an argument is passed by reference or by pointer and we want to use its initial value
in postcondition, we must **save the initial value** then **restore it later**, of course!
The framework offers few macros that will save/restore initial values:

* ```CONTRACT_SAVE_VAL( x )``` will save the argument ```x``` initial value.
* ```CONTRACT_SAVE_PTR_VAL( p )``` will save the argument p initial value, ```*p```.
* ```CONTRACT_SAVE_PTR_PTR_VAL( pp )``` will save the argument pp initial value ```**p```.

In order to use these values during postcondition evaluations, use these macros:

* ```CONTRACT_IN_VAL( x )``` will restore the value of ```x```.
* ```CONTRACT_IN_PTR_VAL( p )``` will restore the value of ```*p```.
* ```CONTRACT_IN_PTR_PTR_VAL( pp )``` will restore the value of ```**p```.
  
```

#!c++

/**
 * \param a is a value by reference
 * \param p is a pointer
 *
 * \return a
 *
 * \post returned value = initial a
 * \post initial a == final a /2
 * \post final *p == initial *p
 */
CONTRACT_FUNC( int, func1( int& a, int* p ) )
CONTRACT_SAVE_VAL( a )
CONTRACT_SAVE_PTR_VAL( p )
CONTRACT_ENSURE( CONTRACT_RET_VAL == CONTRACT_IN_VAL(a), CONTRACT_RET_VAL, CONTRACT_IN_VAL(a) )
CONTRACT_ENSURE( CONTRACT_IN_VAL(a) == a/2, CONTRACT_IN_VAL(a), a )
CONTRACT_ENSURE( *p == CONTRACT_IN_PTR_VAL(p), *p, CONTRACT_IN_PTR_VAL(p) )
CONTRACT_BEGIN
{
    int r = a;
    a *= 2;

    return r;
}
CONTRACT_END

```

### How do I choose the program behavior on condition failure? ###

When a pre- or postcondition fails, the program can have three different behaviors:

1. Throwing an exception reporting the failure;
2. Reporting the failure then aborting; 
3. Just reporting the failure.

We can select the behavior by defining one or more of these macros before the ```#include "contracts.h"```
line.

* ```CONTRACT_ABORT_ON_PRECONDITION_FAILURE```
* ```CONTRACT_THROW_ON_PRECONDITION_FAILURE``` 
* ```CONTRACT_ABORT_ON_POSTCONDITION_FAILURE```
* ```CONTRACT_THROW_ON_POSTCONDITION_FAILURE``` 

When neither ```_ABORT_``` nor ```_THROW_``` macro is/are defined, the default behavior is to
display the error on console then continue.

### How do I set invariant conditions in a class? ###

It is possible to check for **class** and/or **object/instance** invariants in the code. Here is
a simple class declared with invariant conditions:

```

#!c++

/**
 * \file Simple.h
 */

#ifndef __SIMPLE_H__
#define __SIMPLE_H__

#define CONTRACT_DEFINE_INVARIANTS
#include "contracts.h"

/**
 * \class Simple
 */
CONTRACT_CLASS_WITH_INVARIANTS( Simple )
CONTRACT_INSTANCE_INVARIANT( THIS->mMember < 10, THIS->mMember )
CONTRACT_INSTANCE_INVARIANT( THIS->mMember < Simple::msMember, THIS->mMember, Simple::msMember )
CONTRACT_CLASS_INVARIANT( Simple::smMember < 2, Simple::smMember )
CONTRACT_CLASS_DECLARATION_BEGIN
public:
    virtual ~Simple(){}
    Simple();

    // ...

protected:
    int mMember;
    static int msMember;
CONTRACT_CLASS_DECLARATION_END

#endif // __SIMPLE_H__
```

First of all, one must ```#define CONTRACT_DEFINE_INVARIANTS``` before including ```contracts.h```.
This will allow to define some static members that **must not** be defined again when including
the class declaration file (here: ```Simple.h```). Thus, ```CONTRACT_DEFINE_INVARIANTS``` must be
defined only in the class header file.

* ```CONTRACT_CLASS_WITH_INVARIANTS( className )``` declares the class with contract.
* ```CONTRACT_INSTANCE_INVARIANT( condition, variable1[, variable2, ...] )``` declares an **instance** invariant,
i.e. an invariant that must be respected by any instance of this class. In the condition,
```THIS``` can be used as a pointer to the instance (like ```this``` in the standard code.)
* ```CONTRACT_CLASS_INVARIANT( condition, variable1[, variable2, ...] )``` declares a **class** invariant,
i.e. an invariant that must be respected by the class itself, i.e. by its static methods.
* ```CONTRACT_CLASS_DECLARATION_BEGIN``` marks the begining of the class definition. Don't use curly brace
after this macro.
* ```CONTRACT_CLASS_DECLARATION_END``` marks the end of the class definition. Agin, don't use curly brace before
this macro.

If one wants to use contract with a derived class, proceed like this:

```

#!c++

#include "Base.h"

#define CONTRACT_DEFINE_INVARIANTS
#include "contracts.h"

CONTRACT_DERIVED_CLASS_WITH_INVARIANTS( Derived, public Base )
//...
```

Here, ```Derived``` is the name of the derived class, and ```public Base``` means
a public inheritance from base class ```Base```.

---------------------------------

### Strong points ###
* Easy to use (a single header file).
* Quite detailed breach of contract messages. Better than ```assert()```.
* The preconditions are evaluated **before** entering the function body and the postcondition are evaluated **after** exiting the function body.
* The invariants are verified **before** entering the method and **after** exiting it.

### Weaknesses ###
* Not coded in documentation header, need additional documentation (use \pre, \post or \invariant in Doxygen tags...).
* Quite slow (but who could expect faster code...)

### Author? ###

* Repo owner : lgd.beauchamp (at) gmail...