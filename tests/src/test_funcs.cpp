#include <iostream>
#include <stdexcept>

#include "../../include/contracts.h"

#include <boost/current_function.hpp>

using namespace std;

//------------------------------------------------------------------------------

/**
 * void() test function.
 */
CONTRACT_FUNC( void, void_to_void_func() )
CONTRACT_BEGIN
{
    cout << "    From inside " << BOOST_CURRENT_FUNCTION << endl;
    cout << "    void() function will never breach pre- or postconditions." << endl;
    cout << endl;
}
CONTRACT_END_VOID

//------------------------------------------------------------------------------

/**
 * int() test function.
 * 
 * @return an integer.
 * 
 * @post returned value must be < 10.
 */
CONTRACT_FUNC( int, void_to_int_func() )
CONTRACT_ENSURE( CONTRACT_RET_VAL < 10, CONTRACT_RET_VAL )
CONTRACT_BEGIN
{
    cout << "    From inside " << BOOST_CURRENT_FUNCTION << endl;
    cout << "    int() function can only breach postconditions." << endl;
    cout << endl;

    return 10;
}
CONTRACT_END

//------------------------------------------------------------------------------

/**
 * double(double) test function.
 * 
 * @param a is a number
 * 
 * @return a function of the number
 * 
 * @pre The input number must be >= 0.
 * @post The returned value must be < 10.
 */
CONTRACT_FUNC( double, double_to_double_func( double a ) )
CONTRACT_EXPECT( a >= 0, a )
CONTRACT_ENSURE( CONTRACT_RET_VAL < 10, CONTRACT_RET_VAL )
CONTRACT_BEGIN
{
    cout << "    From inside " << BOOST_CURRENT_FUNCTION << endl;
    cout << "    double(double) function can breach pre- and postconditions." << endl;
    cout << endl;

    return a*-10.;
}
CONTRACT_END

//------------------------------------------------------------------------------

/**
 * double(double) function, post condition including input value.
 * 
 * @param a is a number
 * 
 * @return function of a
 * 
 * @pre The input number is >= 0.
 * @post The returned value is < 10. && < input value
 */
CONTRACT_FUNC( double, double_to_double_func2( double a ) )
CONTRACT_SAVE_VAL( a )
CONTRACT_EXPECT( a >= 0, a )
CONTRACT_ENSURE( CONTRACT_RET_VAL < 10, CONTRACT_RET_VAL )
CONTRACT_ENSURE( CONTRACT_RET_VAL < CONTRACT_IN_VAL( a ), CONTRACT_RET_VAL, CONTRACT_IN_VAL( a ) )
CONTRACT_BEGIN
{
    cout << "    From inside " << BOOST_CURRENT_FUNCTION << endl;
    cout << "    double(double) function can breach pre- and postconditions." << endl;
    cout << endl;

    return a*-20;
}
CONTRACT_END

//------------------------------------------------------------------------------

/**
 * void(double&) return a value by reference.
 * 
 * @param a have a value on in, will be set on return
 * 
 * @pre a < 10
 * @post a = 2* value in input
 */
CONTRACT_FUNC( void, double_ref_to_void_func( double& a ) )
CONTRACT_SAVE_VAL( a )
CONTRACT_EXPECT( a < 10, a )
CONTRACT_ENSURE( a == 2*CONTRACT_IN_VAL( a ), a, CONTRACT_IN_VAL( a ) )
CONTRACT_BEGIN
{
    cout << "    From inside " << BOOST_CURRENT_FUNCTION << endl;
    cout << "    void(double&) function can breach pre- and postconditions." << endl;
    cout << endl;
    
    a *=3;
}
CONTRACT_END_VOID

//------------------------------------------------------------------------------

/**
 * void(double*) return a value by pointer.
 * 
 * @param a points to a double that has a value on in, will be set on return
 * 
 * @pre *a < 10
 * @post *a = 2* value in input
 */
CONTRACT_FUNC( void, double_ptr_to_void_func( double* a ) )
CONTRACT_SAVE_PTR_VAL( a )
CONTRACT_EXPECT( *a < 10, *a )
CONTRACT_ENSURE( *a == 2*CONTRACT_IN_PTR_VAL( a ), *a, CONTRACT_IN_PTR_VAL( a ) )
CONTRACT_BEGIN
{
    cout << "    From inside " << BOOST_CURRENT_FUNCTION << endl;
    cout << "    void(double*) function can breach pre- and postconditions." << endl;
    cout << endl;
    
    *a *=3;
}
CONTRACT_END_VOID

//------------------------------------------------------------------------------

/**
 * int*(int**) will allocate the pointer _and_ return it.
 * 
 * @param p is a ptr to ptr int to be allocated inside the function.
 * 
 * @return *p, allocated pointer
 * 
 * @pre *p is nullptr
 * @post *p is not nullptr
 */
CONTRACT_FUNC( int*, intptrptr_to_intptr_func(int** pp ) )
CONTRACT_EXPECT( *pp == nullptr, *pp )
CONTRACT_ENSURE( CONTRACT_RET_VAL != nullptr, CONTRACT_RET_VAL )
CONTRACT_BEGIN
{
    cout << "    From inside " << BOOST_CURRENT_FUNCTION << endl;
    cout << "    int*(int**) function can breach pre- and postconditions." << endl;
    cout << endl;   
    
    //*pp = new int;
    *pp = nullptr;
    
    return *pp;
}
CONTRACT_END

//------------------------------------------------------------------------------

/**
 * Precondition breach only
 * 
 * @pre precondition will always fail
 */
CONTRACT_FUNC( void, precondition_breach() )
CONTRACT_EXPECT( false == true, false, true )
CONTRACT_BEGIN
{
}
CONTRACT_END_VOID

//------------------------------------------------------------------------------

/**
 * Postcondition breach only
 * 
 * @post postcondition will always fail
 */
CONTRACT_FUNC( int, postcondition_breach() )
CONTRACT_ENSURE( false == true, false, true )
CONTRACT_BEGIN
{
    return 0;
}
CONTRACT_END

//==============================================================================

void test_funcs()
{
    cout << BOOST_CURRENT_FUNCTION << endl;

    try
    {
        cout << endl;
        cout << "    //--------------------" << endl;
        void_to_void_func();

        cout << endl;
        cout << "    //--------------------" << endl;
        void_to_int_func();

        cout << endl;
        cout << "    //--------------------" << endl;
        double_to_double_func( -1 );

        cout << endl;
        cout << "    //--------------------" << endl;
        double_to_double_func2( -1 );

        cout << endl;
        cout << "    //--------------------" << endl;
        double a = 11;
        double_ref_to_void_func( a );
        
        cout << endl;
        cout << "    //--------------------" << endl;
        double b = 11;
        double_ptr_to_void_func( &b );

        cout << endl;
        cout << "    //--------------------" << endl;
        int tmp;
        int* p = &tmp;  // not nullptr
        int* r = intptrptr_to_intptr_func( &p );
        delete p;
    }
    catch( logic_error& err )
    {
        cerr << err.what() << endl;
    }
    
    cout << endl;
    cout << "    //--------------------" << endl;
    try
    {
        precondition_breach();
    }
    catch ( CONTRACT_PRECONDITION_EXCEPTION& err )
    {
        cerr << err.what() << endl;
    }

    cout << endl;
    cout << "    //--------------------" << endl;
    try
    {
        postcondition_breach();
    }
    catch ( CONTRACT_POSTCONDITION_EXCEPTION& err )
    {
        cerr << err.what() << endl;
    }

    cout << endl;
}
