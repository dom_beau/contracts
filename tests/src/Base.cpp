#include "../include/Base.h"

//------------------------------------------------------------------------------

Base::Base() : 
    mValue( 10 )
{
    CONTRACT_ENFORCE_INVARIANTS();
}

//------------------------------------------------------------------------------

CONTRACT_FUNC( void, Base::set( int pArg ) )
CONTRACT_BEGIN_ENFORCE_INVARIANTS
{
    mValue = pArg;
}
CONTRACT_END_VOID_ENFORCE_INVARIANTS
    
//------------------------------------------------------------------------------
    