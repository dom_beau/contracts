#include "../include/Derived.h"

//------------------------------------------------------------------------------

Derived::Derived() : 
    Base()
{
    CONTRACT_ENFORCE_INVARIANTS();
}

//------------------------------------------------------------------------------

CONTRACT_FUNC( void, Derived::set( int pArg ) )
CONTRACT_BEGIN_ENFORCE_INVARIANTS
{
    Base::set( pArg );
}
CONTRACT_END_VOID_ENFORCE_INVARIANTS

//------------------------------------------------------------------------------
