#include "../include/Simple.h"

#include <iostream>

#include <boost/current_function.hpp>

using namespace std;

void useSimpleClass()
{
    cout << BOOST_CURRENT_FUNCTION << endl;
        
    Simple S;
    
    S.set( 20 );
}
