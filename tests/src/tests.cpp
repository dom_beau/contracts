// Contract test for functions
#include <iostream>
#include <stdexcept>
#include <cstring>

#define DEFINE_CONTRACTS
#include "../../include/contracts.h"

using namespace std;

extern void test_funcs();
extern void test_classes( int type );
extern void useSimpleClass();

//==============================================================================

void help()
{
    cout << "Tests for Programmation by Contract" << endl << endl;
    cout << "$ progbycontract functions : will test pre- and postconditions on standalone functions." << endl;
    cout << "$ progbycontract simpleclass : will test invariants on a simple class." << endl;
    cout << "$ progbycontract derivedclass : will test invariants on an inheritance of classes." << endl;
    cout << endl;
}

//------------------------------------------------------------------------------

int main( int argc, char* argv[] )
{
    if ( argc < 2 )
    {
        help();
        exit( 0 );
    }
    
    cout << endl;
    cout << "Contract tests..." << endl;
    cout << "=================" << endl;
    cout << endl;
    
    if ( strcmp( argv[1], "functions" ) == 0 )
    {
        test_funcs();
        exit( 0 );
    }
    
    if ( strcmp( argv[1], "simpleclass" ) == 0 )
    {
        test_classes( 0 );
        exit( 0 );
    }

    if ( strcmp( argv[1], "derivedclass" ) == 0 )
    {
        test_classes( 1 );
        exit( 0 );
    }
    
    useSimpleClass();
}
