#include "../include/Simple.h"
#include "../include/Base.h"
#include "../include/Derived.h"

#include "../../include/contracts.h"

#include <iostream>
#include <stdexcept>

#include <boost/current_function.hpp>

using namespace std;

//==============================================================================

// type:
// 0 = simple
// 1 = inheritance
void test_classes( int type )
{
    cout << BOOST_CURRENT_FUNCTION << endl;

    try
    {
        if ( type == 0 )
        {
            cout << endl;
            cout << "    //--------------------" << endl;
            cout << "    Create a Simple object (call cTor)" << endl;

            Simple simple;

            cout << endl;
            cout << "    //--------------------" << endl;
            cout << "    Call instance method simple.set( 12 )" << endl;

            simple.set( 12 );

            cout << endl;
            cout << "    //--------------------" << endl;
            cout << "    Call instance method simple.get()" << endl;

            simple.get();

            cout << endl;
            cout << "    //--------------------" << endl;
            cout << "    Call class method Simple::sset( 12 )" << endl;

            Simple::sset( 12 );
        }
        else if ( type == 1 )
        {
            cout << endl;
            cout << "    //--------------------" << endl;
            cout << "    Create a Base object (call cTor)" << endl;
            
            Base base;

            cout << endl;
            cout << "    //--------------------" << endl;
            cout << "    Call instance method base.set( 12 )" << endl;

            base.set( 12 );

            cout << endl;
            cout << "    //--------------------" << endl;
            cout << "    Dynamic polymorphism with cTor" << endl;

            Derived derived;
            Base* baseptr = &derived;

            derived.set( 20 );
        }
    }
    catch( CONTRACT_INVARIANT_EXCEPTION& err )
    {
        cerr << err.what() << endl;
    }

    cout << endl;
}
