#include "../include/Simple.h"

//------------------------------------------------------------------------------

int Simple::msMember = 2;

//------------------------------------------------------------------------------

Simple::Simple() :
    mMember( 11 )
{
    CONTRACT_ENFORCE_INVARIANTS();
}

//------------------------------------------------------------------------------

CONTRACT_FUNC( void, Simple::set( int pArg ) )
CONTRACT_BEGIN_ENFORCE_INVARIANTS
{
    mMember = pArg;
}
CONTRACT_END_VOID_ENFORCE_INVARIANTS

//------------------------------------------------------------------------------

CONTRACT_FUNC_STATIC( Simple, void, Simple::sset( int pArg ) )
CONTRACT_EXPECT( pArg == 0, pArg )
CONTRACT_BEGIN_ENFORCE_INVARIANTS_STATIC
{
    msMember = pArg;
}
CONTRACT_END_VOID_ENFORCE_INVARIANTS_STATIC

//------------------------------------------------------------------------------

CONTRACT_FUNC( int, Simple::get()const )
CONTRACT_ENSURE( CONTRACT_RET_VAL == 0, CONTRACT_RET_VAL )
CONTRACT_BEGIN_ENFORCE_INVARIANTS
{
    return mMember;
}
CONTRACT_END_ENFORCE_INVARIANTS

//------------------------------------------------------------------------------
