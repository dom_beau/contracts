/* 
 * File:   Simple.h
 * Author: dbeauchamp
 *
 * Created on 30 mai 2018, 22:27
 */

#ifndef SIMPLE_H
#define	SIMPLE_H

#define CONTRACT_DEFINE_INVARIANTS
#include "../../include/contracts.h"

CONTRACT_CLASS_WITH_INVARIANTS( Simple )
CONTRACT_INSTANCE_INVARIANT( THIS->mMember < 10, THIS->mMember )
CONTRACT_INSTANCE_INVARIANT( THIS->mMember < Simple::msMember, THIS->mMember, Simple::msMember )
CONTRACT_CLASS_INVARIANT( Simple::msMember < 2, Simple::msMember )
CONTRACT_CLASS_DECLARATION_BEGIN
public:
    virtual ~Simple(){}
    
    Simple();
    
    //--------------------------------------------------------------------------
    
    /**
     * set the value of mMember
     * 
     * @param pArg
     * 
     * @invariant mMember < 10
     */
    void set( int pArg );
    
    /**
     * get the value of mMember
     * 
     * @return pArg
     * 
     * @invariant mMember < 10
     */
    int get() const;
    
    //--------------------------------------------------------------------------
    
    /**
     * set the value of msMember
     * 
     * @param pArg
     * 
     * @invariant msMember < 2
     */
    static void sset( int pArg );
    
protected:
    int mMember;
    static int msMember;
CONTRACT_CLASS_DECLARATION_END;

#endif	/* SIMPLE_H */

