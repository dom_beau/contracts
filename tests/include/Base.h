/* 
 * File:   Base.h
 * Author: dbeauchamp
 *
 * Created on 30 mai 2018, 23:48
 */

#ifndef BASE_H
#define	BASE_H

#define CONTRACT_DEFINE_INVARIANTS
#include "../../include/contracts.h"

CONTRACT_CLASS_WITH_INVARIANTS( Base )
CONTRACT_INSTANCE_INVARIANT( THIS->mValue < 10, THIS->mValue )
CONTRACT_CLASS_DECLARATION_BEGIN
public:
    virtual ~Base(){}
    Base();
    
    /**
     * Set mValue
     * 
     * @param pArg
     * 
     * @invariant mValue < 10
     */
    virtual void set( int pArg );
    
protected:
    int mValue;
CONTRACT_CLASS_DECLARATION_END

#endif	/* BASE_H */

