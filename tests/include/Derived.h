/* 
 * File:   Derived.h
 * Author: dbeauchamp
 *
 * Created on 30 mai 2018, 23:55
 */

#ifndef DERIVED_H
#define	DERIVED_H

#include "Base.h"

#define CONTRACT_DEFINE_INVARIANTS
#include "../../include/contracts.h"

//------------------------------------------------------------------------------

CONTRACT_DERIVED_CLASS_WITH_INVARIANTS( Derived, public Base )
CONTRACT_INSTANCE_INVARIANT( THIS->mValue < 5, THIS->mValue )
CONTRACT_CLASS_DECLARATION_BEGIN
public:
    virtual ~Derived(){}
    Derived();
    
    /**
     * Set 
     * 
     * @param pArg
     * 
     * @invariant Base::mValue < 5
     */
    virtual void set( int pArg );
    
CONTRACT_CLASS_DECLARATION_END

#endif	/* DERIVED_H */

