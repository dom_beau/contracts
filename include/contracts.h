/// \file contracts.h 
///
/// \author Dominique Beauchamp
/// \date 2018-05-29
/// \version 0.3 alpha
/// \copyright Dominique Beauchamp
/// 
/// Used #define:
///
/// - USE_CONTRACTS -> If defined, will enforce contract conditions. If not defined,
///  contract code will not be added to normal code.
///
///  Use -DUSE_CONTRACTS to allow definition to be propagated to every translation unit.
///
/// - DEFINE_CONTRACTS -> must be defined only in the "first" (where main() is)
///  translation unit to allow some static code to be defined in this file.
#if !defined( __CONTRACTS_H__ )
#define __CONTRACTS_H__

// Make sure the compiler supports C++11
#if __cplusplus < 201103L
    #error C++11 not supported!
#endif

//------------------------------------------------------------------------------

/// Requested headers
///
/// These headers will be included only if USE_CONTRACTS is defined to avoid
/// including to many files if not requested.
#if defined( USE_CONTRACTS )
#
#   include <cassert>
#   include <cstdlib>
#   include <functional>
#   include <iostream>
#   include <memory>
#   include <mutex>
#   include <sstream>
#   include <stdexcept>
#   include <type_traits>
#   include <vector>
#
#   include <boost/current_function.hpp>
#   include <boost/preprocessor/seq/for_each.hpp>
#   include <boost/preprocessor/stringize.hpp>
#   include <boost/preprocessor/variadic/to_seq.hpp>
#
#endif

//------------------------------------------------------------------------------

/// Terminal color code for condition failure output.
/// @see https://stackoverflow.com/questions/2616906/how-do-i-output-coloured-text-to-a-linux-terminal
#if defined( USE_CONTRACTS )
#
#define ABORT_COLOR "\033[0;31;49m"
#define WARN_COLOR "\033[0;93;49m"
#define DEF_COLOR "\033[0m"
#
#endif

//------------------------------------------------------------------------------

/// Namespace contract_23a2aafa07ed defined only if USE_CONTRACTS is defined.
#if defined( USE_CONTRACTS )
#
    namespace contract_23a2aafa07ed
    {
        /// \class Mutex
        ///
        /// Prevent two or more failures from two or more threads to write to
        /// the console at the same time.
        class Mutex
        {
        public:
            typedef std::mutex type;

            static type globalContractMutex;
        };
        
        // must define DEFINE_CONTRACTS once in the main.cpp
#if defined( DEFINE_CONTRACTS )
        std::mutex Mutex::globalContractMutex;
#endif

        /// \class PreconditionException
        /// Object of this class can be thrown on precondition failure. This
        /// class inherits std::logic_error.
        class PreconditionException : public std::logic_error
        {
        public:
            PreconditionException( const char* ppMsg ) : 
                std::logic_error( ppMsg )
                {}
        };
        
        /// \class PostconditionException
        /// Object of this class can be thrown on postcondition failure. This
        /// class inherits std::logic_error.
        class PostconditionException : public std::logic_error
        {
        public:
            PostconditionException( const char* ppMsg ) : 
                std::logic_error( ppMsg )
                {}
        };
        
        /// \class InvariantException
        /// Object of this class can be thrown on invariant failure. This
        /// class inherits std::logic_error.
        class InvariantException : public std::logic_error
        {
        public:
            InvariantException( const char* ppMsg ) : 
                std::logic_error( ppMsg )
                {}
        };
    }
    
/// When USE_CONTRACTS is defined, contract condition failure macros are defined
/// using these classes...
#define CONTRACT_PRECONDITION_EXCEPTION ::contract_23a2aafa07ed::PreconditionException
#define CONTRACT_POSTCONDITION_EXCEPTION ::contract_23a2aafa07ed::PostconditionException
#define CONTRACT_INVARIANT_EXCEPTION ::contract_23a2aafa07ed::InvariantException
#
#else
#
/// When USE_CONTRACTS is not defined, contract condition failure macros are defined
/// directly to std::logic_error
#define CONTRACT_PRECONDITION_EXCEPTION std::logic_error
#define CONTRACT_POSTCONDITION_EXCEPTION std::logic_error
#define CONTRACT_INVARIANT_EXCEPTION std::logic_error
#
#endif

//------------------------------------------------------------------------------

/// Action to perform when precondition fails
///
/// #define CONTRACT_ABORT_ON_PRECONDITION_FAILURE -> will write error message 
///     in the console then call abort().
/// #define CONTRACT_THROW_ON_PRECONDITION_FAILURE -> will throw a 
////    CONTRACT_PRECONDITION_EXCEPTION with error message.
/// otherwise, will only write a message in the console.
#if defined( USE_CONTRACTS )
#   if defined( CONTRACT_ABORT_ON_PRECONDITION_FAILURE )
#       // Write breach of precondition then abort
#       define PRECONDITION_ACTION( cond, func, file, line, ... )\
            std::cerr << ABORT_COLOR << "*** Precondition failed for condition '"\
                << #cond << "' in '" << func << "', " << file << " : " << line\
                CONTRACT_WRITE_VARS( __VA_ARGS__ )\
                << DEF_COLOR << std::endl;\
            abort();
#   elif defined( CONTRACT_THROW_ON_PRECONDITION_FAILURE )
#       // Throw with breach of precondition message
#       define PRECONDITION_ACTION( cond, func, file, line, ... )\
            std::ostringstream lOss;\
            lOss << "*** Precondition failed for condition '"\
                << #cond << "' in '" << func << "', " << file << " : " << line\
                CONTRACT_WRITE_VARS( __VA_ARGS__ )\
                << std::ends;\
            throw contract_23a2aafa07ed::PreconditionException( lOss.str().c_str() );
#   else
#       // Write breach of precondition but don't abort
#       define PRECONDITION_ACTION( cond, func, file, line, ... )\
            std::cerr << WARN_COLOR << "*** Precondition failed for condition '"\
                << #cond << "' in '" << func << "', " << file << " : " << line\
                CONTRACT_WRITE_VARS( __VA_ARGS__ )\
                << DEF_COLOR << std::endl;            
#   endif
#else
#endif
             
//------------------------------------------------------------------------------

/// Action to perform when postcondition fails
///
/// #define CONTRACT_ABORT_ON_POSTCONDITION_FAILURE -> will write error message 
///     in the console then call abort().
/// #define CONTRACT_THROW_ON_POSTCONDITION_FAILURE -> will throw a 
////    CONTRACT_POSTCONDITION_EXCEPTION with error message.
/// otherwise, will only write a message in the console.
#if defined( USE_CONTRACTS )
#   if defined( CONTRACT_ABORT_ON_POSTCONDITION_FAILURE )
#       // Write breach of postcondition then abort
#       define POSTCONDITION_ACTION( cond, func, file, line, ... )\
            std::cerr << ABORT_COLOR << "*** Postcondition failed for condition '"\
                << #cond << "' in '" << func << "', " << file << " : " << line\
                CONTRACT_WRITE_VARS( __VA_ARGS__ )\
                << DEF_COLOR << std::endl;\
            abort();
#   elif defined( CONTRACT_THROW_ON_POSTCONDITION_FAILURE )
#       // Throw with breach of postcondition message
#       define POSTCONDITION_ACTION( cond, func, file, line, ... )\
            std::ostringstream lOss;\
            lOss << "*** Postcondition failed for condition '"\
                << #cond << "' in '" << func << "', " << file << " : " << line\
                CONTRACT_WRITE_VARS( __VA_ARGS__ )\
                << std::ends;\
            throw contract_23a2aafa07ed::PostconditionException( lOss.str().c_str() );
#   else
#       // Write breach of postcondition but don't abort
#       define POSTCONDITION_ACTION( cond, func, file, line, ... )\
            std::cerr << WARN_COLOR << "*** Postcondition failed for condition '"\
                << #cond << "' in '" << func << "', " << file << " : " << line\
                CONTRACT_WRITE_VARS( __VA_ARGS__ )\
                << DEF_COLOR << std::endl;            
#   endif
#else
#endif

//------------------------------------------------------------------------------

/// Action to perform when invariant check fails
///
/// #define CONTRACT_ABORT_ON_INVARIANT_CHECK_FAILURE -> will write error message 
///     in the console then call abort().
/// #define CONTRACT_THROW_ON_INVARIANT_CHECK_FAILURE -> will throw a 
////    CONTRACT_INVARIANT_EXCEPTION with error message.
/// otherwise, will only write a message in the console.
#if defined( USE_CONTRACTS )
#   if defined( CONTRACT_ABORT_ON_INVARIANT_CHECK_FAILURE )
#       // Write breach of invariant check then abort
#       define INVARIANT_CHECK_ACTION( cond, func, file, line, ... )\
            std::cerr << ABORT_COLOR << "*** Invariant check failed for condition '"\
                << #cond << "' in '" << func << "', " << file << " : " << line\
                CONTRACT_WRITE_VARS( __VA_ARGS__ )\
                << DEF_COLOR << std::endl;\
            abort();
#   elif defined( CONTRACT_THROW_ON_INVARIANT_CHECK_FAILURE )
#       // Throw with breach of invariant check message
#       define INVARIANT_CHECK_ACTION( cond, func, file, line, ... )\
            std::ostringstream lOss;\
            lOss << "*** Invariant check failed for condition '"\
                << #cond << "' in '" << func << "', " << file << " : " << line\
                CONTRACT_WRITE_VARS( __VA_ARGS__ )\
                << std::ends;\
            throw contract_23a2aafa07ed::PostconditionException( lOss.str().c_str() );
#   else
#       // Write breach of invariant check but don't abort
#       define INVARIANT_CHECK_ACTION( cond, func, file, line, ... )\
            std::cerr << WARN_COLOR << "*** Invariant check failed for condition '"\
                << #cond << "' in '" << func << "', " << file << " : " << line\
                CONTRACT_WRITE_VARS( __VA_ARGS__ )\
                << DEF_COLOR << std::endl;            
#   endif
#else
#endif

//------------------------------------------------------------------------------

/// \note Internal use only: write variable names and values for display in 
/// case of breach of contract.
#if defined( USE_CONTRACTS )
#   define _CONTRACT_WRITE_VAR( r, data, elem )\
        << std::endl << "    -> " << BOOST_PP_STRINGIZE( elem ) << " = " << elem
#
#   define CONTRACT_WRITE_VARS( ... )\
        BOOST_PP_SEQ_FOR_EACH(\
            _CONTRACT_WRITE_VAR,\
            ,\
            BOOST_PP_VARIADIC_TO_SEQ( __VA_ARGS__ )\
        )
#
#endif

//------------------------------------------------------------------------------

/// Function definition
/// 
/// Only defined when USE_CONTRACTS is defined.
///
/// CONTRACT_FUNC( ret_t, signature )
/// CONTRACT_BEGIN
/// CONTRACT_END / CONTRACT_END_VOID
///
/// \note Use CONTRACT_END for functions that return something and CONTRACT_END_VOID
/// when ret_t is void.
    
// We must give ret_t explicitely to be able to define postcondition lambdas
// before calling the actual function (and thus getting its type via a
// decltype()).
// 
// Note that we must use either CONTRACT_END or CONTRACT_END_VOID depending on
// whether the function returns a value or not (void).
#if defined( USE_CONTRACTS )
#   define CONTRACT_FUNC( ret_t, signature )\
        ret_t signature\
        {\
            typedef ret_t contract_23a2aafa07ed_ret_t;\
            const char* contract_23a2aafa07ed_function_signature =\
                BOOST_CURRENT_FUNCTION;\
            std::vector<std::function<void()>>\
                contract_23a2aafa07ed_preconditions;\
            std::vector<std::function<void(contract_23a2aafa07ed_ret_t*)>>\
                contract_23a2aafa07ed_postconditions;             
#
#   define CONTRACT_BEGIN\
        for ( auto c : contract_23a2aafa07ed_preconditions ) c();\
        auto contract_23a2aafa07ed_fct = [&]()
#
#   define CONTRACT_END\
        ;\
        contract_23a2aafa07ed_ret_t _ret = contract_23a2aafa07ed_fct();\
        for ( auto c : contract_23a2aafa07ed_postconditions ) c( &_ret );\
        return _ret;\
    }
#
#   define CONTRACT_END_VOID\
        ;\
        contract_23a2aafa07ed_fct();\
        for ( auto c : contract_23a2aafa07ed_postconditions ) c( nullptr );\
        return;\
    }   
#else
#   define CONTRACT_FUNC( ret_t, signature ) ret_t signature
#   define CONTRACT_BEGIN
#   define CONTRACT_END
#   define CONTRACT_END_VOID
#endif

//------------------------------------------------------------------------------

/// Save the initial values (usually function parameters) to use them at function
/// exit for ensureing postconditions.
///
/// CONTRACT_SAVE_VAL( x ) will save the value of "x" (x by copy or by reference)
///     by calling its operator=().
/// CONTRACT_IN_VAL( x ) will restore the initial value of "x" to use it in
///     postconditions.
///
/// CONTRACT_SAVE_PTR_VAL( x ) will save the value of "*x" by calling operator=().
/// CONTRACT_IN_PTR_VAL( x ) will restore the initial value of "*x" to use it in
///     postconditions.
///
/// CONTRACT_SAVE_PTR_PTR_VAL( x ) will save the value of "**x" by calling operator=().
/// CONTRACT_IN_PTR_PTR_VAL( x ) will restore the initial value of "**x" to use it in
///     postconditions.
///
#if defined( USE_CONTRACTS )
#   define CONTRACT_SAVE_VAL( x )\
            const std::remove_reference<decltype(x)>::type _in_##x = x;
#   define CONTRACT_SAVE_PTR_VAL( x )\
        const std::remove_pointer<decltype(x)>::type _in_ptr_value_##x = *x;
#   define CONTRACT_SAVE_PTR_PTR_VAL( x )\
        const std::remove_pointer<std::remove_pointer<decltype(x)>::type>::type\
            _in_ptr_ptr_value_##x = **x;
#
#   define CONTRACT_IN_VAL( x ) _in_##x  
#   define CONTRACT_IN_PTR_VAL( x ) _in_ptr_value_##x
#   define CONTRACT_IN_PTR_PTR_VAL( x ) _in_ptr_ptr_value_##x
#else
#   define CONTRACT_SAVE_VAL( x )
#   define CONTRACT_SAVE_PTR_VAL( x )
#   define CONTRACT_SAVE_PTR_PTR_VAL( x )
#
#   define CONTRACT_IN_VAL( x )
#   define CONTRACT_IN_PTR_VAL( x )
#   define CONTRACT_IN_PTR_PTR_VAL( x )
#endif

//------------------------------------------------------------------------------
    
/// Define a precondition
///
/// CONTRACT_EXPECT( condition, ... ) where variadic arguments are the variable
///     name(s) to be displayed in case of failure.
/// ex.: CONTRACT_EXPECT( a == 0 && b == 1, a, b )
#if defined( USE_CONTRACTS )
#   define CONTRACT_EXPECT( condition, ... )\
        contract_23a2aafa07ed_preconditions.push_back( [&]()\
            {\
                if ( !( condition ) )\
                {\
                    std::lock_guard<contract_23a2aafa07ed::Mutex::type> lock(\
                        ::contract_23a2aafa07ed::Mutex::globalContractMutex\
                    );\
                    PRECONDITION_ACTION(\
                        condition,\
                        contract_23a2aafa07ed_function_signature,\
                        __FILE__,\
                        __LINE__,\
                        __VA_ARGS__\
                    );\
                }\
            }\
        );
#else
#   define CONTRACT_EXPECT( condition, ... )
#endif        
        
//------------------------------------------------------------------------------
    
/// Define a postcondition
///
///
/// CONTRACT_ENSURE( condition, ... ) where variadic arguments are the variable
///     name(s) to be displayed in case of failure.
/// ex.: CONTRACT_ENSURE( CONTRACT_RET_VAL == 0 && CONTRACT_IN_VAL(a) == a, 
///     CONTRACT_RET_VAL, CONTRACT_IN_VAL(a), a )
#if defined( USE_CONTRACTS )
#   define CONTRACT_ENSURE( condition, ... )\
        contract_23a2aafa07ed_postconditions.push_back( [&](\
                contract_23a2aafa07ed_ret_t* _ret_ptr\
            )\
            {\
                if ( !( condition ) )\
                {\
                    std::lock_guard<contract_23a2aafa07ed::Mutex::type> lock(\
                        ::contract_23a2aafa07ed::Mutex::globalContractMutex\
                    );\
                    POSTCONDITION_ACTION(\
                        condition,\
                        contract_23a2aafa07ed_function_signature,\
                        __FILE__,\
                        __LINE__,\
                        __VA_ARGS__\
                    );\
                }\
            }\
        );
#else
#   define CONTRACT_ENSURE( condition, ... )
#endif

//------------------------------------------------------------------------------

/// Gets the function return value in postconditions.
#if defined( USE_CONTRACTS )        
#   define CONTRACT_RET_VAL (*_ret_ptr)
#else
#define CONTRACT_RET_VAL
#endif

////////////////////////////////////////////////////////////////////////////////
/// Invariants

/// Declare a class with invariants
#if defined( USE_CONTRACTS )
#   if defined( CONTRACT_DEFINE_INVARIANTS )
#       define CONTRACT_CLASS_WITH_INVARIANTS( class_t )\
            class class_t\
            {\
            private:\
                typedef class_t THIS_t;\
                static void contract_23a2aafa07ed_check_invariants(\
                    const THIS_t* THIS,\
                    const char* caller,\
                    const char* file,\
                    int line )\
                {
#   else
#       define CONTRACT_CLASS_WITH_INVARIANTS( class_t )\
            class class_t\
            {\
            private:\
                typedef class_t THIS_t;\
                static void contract_23a2aafa07ed_check_invariants();
#   endif
#else
#   define CONTRACT_CLASS_WITH_INVARIANTS( class_t )\
        class class_t\
        {
#endif

//------------------------------------------------------------------------------

#if defined( USE_CONTRACTS )
#   if defined( CONTRACT_DEFINE_INVARIANTS )
#       define CONTRACT_DERIVED_CLASS_WITH_INVARIANTS( class_t, inheritance )\
            class class_t : inheritance\
            {\
            private:\
                typedef class_t THIS_t;\
                static void contract_23a2aafa07ed_check_invariants(\
                    const THIS_t* THIS,\
                    const char* caller,\
                    const char* file,\
                    int line )\
                {
#   else
#       define CONTRACT_DERIVED_CLASS_WITH_INVARIANTS( class_t, inheritance )\
            class class_t : inheritance\
            {\
            private:\
                typedef class_t THIS_t;\
                static void contract_23a2aafa07ed_check_invariants();
#   endif
#else
#   define CONTRACT_DERIVED_CLASS_WITH_INVARIANTS( class_t, inheritance )\
        class class_t\
        {
#endif

//------------------------------------------------------------------------------
            
/// End of class header, begin actual declaration
#if defined( USE_CONTRACTS )
#   if defined( CONTRACT_DEFINE_INVARIANTS )
#       define CONTRACT_CLASS_DECLARATION_BEGIN\
                }
#   else
#       define CONTRACT_CLASS_DECLARATION_BEGIN
#   endif
#else
#   define CONTRACT_CLASS_DECLARATION_BEGIN
#endif

//------------------------------------------------------------------------------

/// End of actual class declaration
#if defined( USE_CONTRACTS )
#   define CONTRACT_CLASS_DECLARATION_END\
        };
#else
#   define CONTRACT_CLASS_DECLARATION_END\
        };
#endif

//------------------------------------------------------------------------------
        
/// Add an instance invariant condition to the class.
/// THIS != nullptr is verified to make sure "this" is valid.
///
/// Example: CONTRACT_INSTANCE_INVARIANT( THIS->a > 10, THIS->a )
///
/// Class invariants can also be checked: ClassName::b == 1
/// 
/// Use this macro to perform instance invariant checks and
/// mix of instance and class invariant checks.
///
/// \note This check will not be performed when called from a
/// class (static) method.
#if defined( USE_CONTRACTS )
#   if defined( CONTRACT_DEFINE_INVARIANTS )
#       define CONTRACT_INSTANCE_INVARIANT( condition, ... )\
            if ( THIS != nullptr && !( condition ) )\
            {\
                std::lock_guard<contract_23a2aafa07ed::Mutex::type> lock(\
                    ::contract_23a2aafa07ed::Mutex::globalContractMutex\
                );\
                INVARIANT_CHECK_ACTION(\
                    condition,\
                    caller,\
                    file,\
                    line,\
                    __VA_ARGS__\
                );\
            }
#   endif
#else
#   define CONTRACT_INSTANCE_INVARIANT( condition, ... )
#endif

//------------------------------------------------------------------------------

/// Add a class invariant condition to the class.
///
/// \note This check will be performed when called from either a class
/// (static) or an instance method.
#if defined( USE_CONTRACTS )
#   if defined( CONTRACT_DEFINE_INVARIANTS )
#       define CONTRACT_CLASS_INVARIANT( condition, ... )\
            if ( !( condition ) )\
            {\
                std::lock_guard<contract_23a2aafa07ed::Mutex::type> lock(\
                    ::contract_23a2aafa07ed::Mutex::globalContractMutex\
                );\
                INVARIANT_CHECK_ACTION(\
                    condition,\
                    caller,\
                    file,\
                    line,\
                    __VA_ARGS__\
                );\
            }
#   endif
#else
#   define CONTRACT_CLASS_INVARIANT( condition, ... )
#endif

//------------------------------------------------------------------------------

/// Explicit check. Can be used in cTors.
///
/// \note Must be used from within a cTor or another _instance_ method
/// since "this" is used.
#if defined( USE_CONTRACTS )
#   define CONTRACT_ENFORCE_INVARIANTS()\
        contract_23a2aafa07ed_check_invariants(\
            this,\
            BOOST_CURRENT_FUNCTION,\
            __FILE__,\
            __LINE__\
        );
#else
#   define CONTRACT_ENFORCE_INVARIANTS()
#endif

//------------------------------------------------------------------------------
        
/// Begin method body and perform invariant checking (for instance method)
#if defined( USE_CONTRACTS )
#   define CONTRACT_BEGIN_ENFORCE_INVARIANTS\
            for ( auto c : contract_23a2aafa07ed_preconditions ) c();\
            contract_23a2aafa07ed_check_invariants(\
                this,\
                BOOST_CURRENT_FUNCTION,\
                __FILE__,\
                __LINE__\
            );\
            auto contract_23a2aafa07ed_fct = [&]()
#else
#   define CONTRACT_BEGIN_ENFORCE_INVARIANTS
#endif
           
//------------------------------------------------------------------------------
            
/// End a method then check for invariants (for instance method). Then return.            
#if defined( USE_CONTRACTS )            
#   define CONTRACT_END_ENFORCE_INVARIANTS\
            ;\
            contract_23a2aafa07ed_ret_t _ret = contract_23a2aafa07ed_fct();\
            contract_23a2aafa07ed_check_invariants(\
            this,\
            BOOST_CURRENT_FUNCTION,\
            __FILE__,\
            __LINE__\
            );\
            for ( auto c : contract_23a2aafa07ed_postconditions ) c( &_ret );\
            return _ret;\
        }
#else
#   define CONTRACT_END_ENFORCE_INVARIANTS
#endif

//------------------------------------------------------------------------------

/// End a method then check for invariants (for instance method). Then return void.            
#if defined( USE_CONTRACTS )
#   define CONTRACT_END_VOID_ENFORCE_INVARIANTS\
            ;\
            contract_23a2aafa07ed_fct();\
            contract_23a2aafa07ed_check_invariants(\
                this,\
                BOOST_CURRENT_FUNCTION,\
                __FILE__,\
                __LINE__\
            );\
            for ( auto c : contract_23a2aafa07ed_postconditions ) c( nullptr );\
            return;\
        } 
#else
#   define CONTRACT_END_VOID_ENFORCE_INVARIANTS
#endif

//------------------------------------------------------------------------------

// We must give ret_t explicitely to be able to define postcondition lambdas
// before calling the actual function (and thus getting its type via a
// decltype()).
#if defined( USE_CONTRACTS )
#   define CONTRACT_FUNC_STATIC( class_t, ret_t, signature )\
        ret_t class_t::signature\
        {\
            typedef class_t THIS_t;\
            typedef ret_t contract_23a2aafa07ed_ret_t;\
            const char* contract_23a2aafa07ed_function_signature =\
                BOOST_CURRENT_FUNCTION;\
            std::vector<std::function<void()>>\
                contract_23a2aafa07ed_preconditions;\
            std::vector<std::function<void(contract_23a2aafa07ed_ret_t*)>>\
                contract_23a2aafa07ed_postconditions;             
#else
#define CONTRACT_FUNC_STATIC( class_t, ret_t, signature )\
        ret_t class_t::signature
#endif

//------------------------------------------------------------------------------

/// Begin method body and perform invariant checking (for class method)
#if defined( USE_CONTRACTS )
#   define CONTRACT_BEGIN_ENFORCE_INVARIANTS_STATIC\
            for ( auto c : contract_23a2aafa07ed_preconditions ) c();\
            THIS_t::contract_23a2aafa07ed_check_invariants(\
                nullptr,\
                BOOST_CURRENT_FUNCTION,\
                __FILE__,\
                __LINE__\
            );\
            auto contract_23a2aafa07ed_fct = [&]()
#else
#   define CONTRACT_BEGIN_ENFORCE_INVARIANTS_STATIC
#endif
           
//------------------------------------------------------------------------------
            
/// End a method then check for invariants (for class method). Then return.            
#if defined( USE_CONTRACTS )            
#   define CONTRACT_END_ENFORCE_INVARIANTS_STATIC\
            ;\
            contract_23a2aafa07ed_ret_t _ret = contract_23a2aafa07ed_fct();\
            THIS_t::contract_23a2aafa07ed_check_invariants(\
                nullptr,\
                BOOST_CURRENT_FUNCTION,\
                __FILE__,\
                __LINE__\
            );\
            for ( auto c : contract_23a2aafa07ed_postconditions ) c( &_ret );\
            return _ret;\
        }
#else
#   define CONTRACT_END_ENFORCE_INVARIANTS_STATIC
#endif

//------------------------------------------------------------------------------

/// End a method then check for invariants (for class method). Then return void.            
#if defined( USE_CONTRACTS )
#   define CONTRACT_END_VOID_ENFORCE_INVARIANTS_STATIC\
            ;\
            contract_23a2aafa07ed_fct();\
            THIS_t::contract_23a2aafa07ed_check_invariants(\
                nullptr,\
                BOOST_CURRENT_FUNCTION,\
                __FILE__,\
                __LINE__\
            );\
            for ( auto c : contract_23a2aafa07ed_postconditions ) c( nullptr );\
            return;\
        } 
#else
#   define CONTRACT_END_VOID_ENFORCE_INVARIANTS_STATIC
#endif

//------------------------------------------------------------------------------

#endif // __CONTRACTS_H__
